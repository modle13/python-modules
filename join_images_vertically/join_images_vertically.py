#!/usr/bin/python3

import collections
import cv2
import numpy
import os
import sys

"""
input param is relative path to dir containing images named with numbers in order to join
e.g.: ['1.png', '2.png']
"""

def vconcat_resize_min(im_list, interpolation=cv2.INTER_CUBIC):
    #w_min = min(im.shape[1] for im in im_list)
    w_min = max(im.shape[1] for im in im_list)
    im_list_resize = [cv2.resize(im, (w_min, int(im.shape[0] * w_min / im.shape[1])), interpolation=interpolation)
                      for im in im_list]
    return cv2.vconcat(im_list_resize)

target_dir = sys.argv[1]

files = os.listdir(target_dir)

cv2_images = []

images_dict = {}

for f in files:
    images_dict[int(f.replace('.png', ''))] = f

for k, v in sorted(images_dict.items()):
    cv2_images.append(cv2.imread(f'{target_dir}/{v}'))

print(cv2_images)

im1 = cv2_images[0]
im2 = cv2_images[1]

#out_image = cv2.vconcat([im2, im2, im2, im2])
#out_image = vconcat_resize_min([im1, im2])
out_image = vconcat_resize_min(cv2_images)
cv2.imwrite('joined.png', out_image)
